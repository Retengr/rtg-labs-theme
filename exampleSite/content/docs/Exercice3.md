---
title: Premiers pas
type: docs
--- 
# Construire une image

## Objectifs de l'exercice

Dans cet exercice, vous allez :

- Construire une image Docker
- Débugger la construction d'une image
- Manipuler les logs d'un conteneur
- Installer un référentiel Docker d'entreprise
- Poussez l'image créée dans le référentiel

## Présentation

![Architecture](../images/Architecture3.png)

En plus d'être un bus de communication très performant, Kafka met à disposition une bibliothèque de composants permettant de connecter ce bus à diverses sources de données (en mode lecture/écriture). Cette bibliothèque s'appelle Kafka Connect.

La société Confluent a apporté plusieurs connecteurs à cette bibliothèque.
Pour les besoins de cette formation, nous utiliserons le connecteur Elasticsearch. 

Concrètement, ce connecteur se présente de la façon suivante :
- L'exécutable (Un fichier jar + script shell qui l'appelle)
- Les dépendances (librairies java : plusieurs fichiers jar)
- Deux fichiers de paramétrage

## Cookbook

- Construire une image à partir d'un dockerfile

    ```shell
    $ docker build -t rtgr/kafka2es . 		 
    ```


## Initialisation 

- Préparation des ressources de notre image : 

    ```shell
    cd
    tar xvf /data/kafka2es.tar .
    ```

## Partie 1 : Création du Dockerfile 

1.	Créez le fichier `~/kafka2es/Dockerfile`. 
1.	L'image se basera sur une image Docker existante qui embarquera la machine virtuelle java. Cherchez sur le docker hub, une image qui répond à cette contrainte
1.	Nous travaillerons dans le répertoire `/opt` du conteneur
1.	Modifiez le Dockerfile afin de télécharger et décompacter Kafka dans le répertoire `/opt` du **conteneur**. Le code ci dessous permet de télécharger kafka en ligne de commande
```bash
 wget http://apache.mediamirrors.org/kafka/1.1.0/kafka_2.11-1.1.0.tgz -O kafka_2.11-1.1.0.tgz
 ```

1.	Modifiez le fichier `connect-standalone.properties` utilisé par notre connecteur pour se connecter à notre conteneur Kafka. Ce fichier devra être copié dans le conteneur. Modifiez le Dockerfile en conséquence
1.	Modifiez le fichier `connect-elasticsearch-sink.properties` utilisé par notre connecteur pour se connecter à notre conteneur Elasticsearch. Ce fichier devra être copié dans le conteneur
1.	 Le fichier `run_standalone.sh` ainsi que l'arborescence `libs` devront être copiés dans le conteneur. Modifiez le `Dockerfile` en conséquence
1.	Le conteneur devra démarrer avec la variable d'environnement "CLASSPATH" positionnée avec la valeur "/opt/libs/kafka-connect-elasticsearch-3.2.0-SNAPSHOT.jar:/opt/kafka_2.11-1.1.0/libs/*:/opt/libs/all. Modifiez le Dockerfile en conséquence 


## Partie 2 : Création de l'image
1.	Utilisez la commande `docker build -t retengr/kafka2es . ` pour créer l'image
1.	Utilisez `docker images` afin de vérifier qu'elle a bien été créée
1.	Démarrez l'image en utilisant le traditionnel `docker run -it retengr/kafka2es`
1.	Justifiez l'erreur de résolution DNS 
1.	Corrigez le problème
1.	Vous pourrez intégrer la phase de build de l'image dans le processus global de docker-compose en utilisant la directive build


## Partie 3 : Vérifiez la création de l'index
Nous venons de démarrer un conteneur exécutant un connecteur compatible "Kafka Connect", la documentation suivante s'applique donc : https://docs.confluent.io/current/connect/restapi.html.

Proposez une solution afin de vérifier que le connecteur vers "Elasticsearch" est actif  

## Partie 4 : Créer un référentiel d'image
1.	Proposez une solution pour installer un "Docker Registry" : https://github.com/docker/distribution
1.	Procédez à l'installation. Vous pouvez installer un "Docker Registry" privé (i.e. sur votre propre machine), ou bien utilisez celui qui pourrait être déployé par votre voisin !
1.	Utilisez "docker push" pour pousser l'image que nous venons de créer dans le référentiel que vous aurez choisi
1.	Proposez votre image à votre voisin!

## Partie 5 : Kibana
Connectez-vous à Kibana (port 5601). 

1.	Déclarez un "index-pattern" ayant la valeur "velib-top". Il s'agit du nom de l'index créée par notre connecteur kafka dans Elasticsearch
1.	Rendez-vous dans l'onglet _visualize_ de Kibana
1.	Créez une nouvelle visualisation de type _metric_ pour commencer
1.	Cette visualisation vous affiche le nombre de messages contenus dans l'index
1.	Essayez de créer une nouvelle visualisation de type _coordinate map_. Que manque t'il à Kibana pour pouvoir fabriquer ce type de représentation à partir des données ?

## Partie 6 : Typage de l'index
Kibana est en mesure d'exploiter de façon puissante les données extraites d'Elasticsearch dans la mesure où il peut les comprendre. Pour l'aider, nous allons donc typer l'index qui a été créée dans Elasticsearch.

Pour faire simple, nous avons choisi ici de supprimer l'index, et de le récréer avec les modifications nécessaires

1.	Mettez le connecteur kafka (kafka2es) en pause : `docker pause kafka2es`
1.	Supprimer l'index en cours `curl -XDELETE 'localhost:9200/velib-topic'`
1.	Recréez-le en utilisant le script `~/kafka2es/create-type.sh`
1.	Redémarrez le connecteur : `docker unpause kafka2es`
1.	Retournez dans Kibana, et recréez l'index `velib-topic`
1.	Kibana doit détecter des informations connues dans l'index, notamment les champs `last_update` et `position` respectivement de type `date` et `geo_point`, ce qui va lui permettre de considérer les informations reçues comme une série temporelle ou comme des données géographiques
1.	Vous devriez maintenant pouvoir créer une visualisation de type `coordinate map`
