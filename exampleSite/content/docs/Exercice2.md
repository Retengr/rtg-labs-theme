---
title: Docker compose
type: docs
--- 

# Docker-compose

## Objectifs de l'exercice

Dans cet exercice, vous allez :

- Mettre en oeuvre deux services pilotés par docker-compose
- Faire communiquer ces deux services
- Mettre en place un outil de monitoring de `docker`

## Présentation

Ce exercice vise à mettre en place la pile logicielle ci-dessous.

<center><img src="../images/containers.png" width=700/>
</center>

Dans un premier temps nous allons nous concentrer sur les composants logiciels `NIFI` et `MONGODB`. Aucune connaissance précise de ces deux composants n'est nécessaire ici.

 Il s'agira de déployer une application qui collectera en continue la disponibilité des vélos en libre-service dans la ville de Toulouse. 
Une fois les données collectées, elles sont stockées dans une base de données MongoDB.


## Partie 1 : Docker-compose

1.	Créez un répertoire "bigdata"
1.	Dans ce répertoire, créez un fichier docker-compose.yml en vous inspirant du modèle suivant : 

    ```
    version: '3.0'
    services:

    nom_du_service1:
        container_name: XXXXXX
        image: XXXXXX
        ports:
        - A:B
        environment:
        VAR: VALUE
        depends_on:
        - other_service_name
    
    ...

    ```

### Un conteneur `nifi`

- Dans le fichier précédemment créé, déclarez un service "nifi" et associez-lui l'image *retengr/nifi-docker:1.0.1*
- Utilisez la commande `docker-compose -f docker-compose.yml up -d` pour démarrer les services (i.e. conteneurs) décrits dans le fichier précédent. La commande `docker-compose down` permet de tuer l'ensemble des conteneurs.


- NIFI est une application web par défaut démarrée sur le port 8080
- Démarrez votre service
- Quelle URL utilisez-vous pour vous y connecter ?

- Analysez les propriétés de la boite intitulée `invokeHTTP`, notamment les onglets `scheduling` et `properties`

- Utilisez le bouton ![play](../images/play.png) pour démarrer les  processeurs `invokeHTTP` et  `putMongo`. 

- Analysez le problème rencontré par MongoDB

### Un conteneur `mongoDB`

- Proposez une solution permettant de régler le problème précédemment constaté, et faire en sorte de faire fonctionner l'insertion dans la base de données MongoDB.

- L'outil `adminMongo` permet de lire les données dans une base de données mongoDB. Proposez une solution pour le mettre en place.


### Partie 4 : Docker UI

Plusieurs interfaces graphiques d'administration Docker existent. 
Vous pouvez utiliser `portainer`, qui s'installe comme un simple conteneur docker


### Partie 5 : Les volumes

- Utilisez la commande `docker kill` pour tuer le conteneur `mongo``
- Redémarrez le en utilisant `docker-compose -f docker-compose.yml up -d`
- Que sont devenues les données qui étaient stockées dans MongoDB ?
- Proposez une solution permettant de remédier à ce problème


## Solution

