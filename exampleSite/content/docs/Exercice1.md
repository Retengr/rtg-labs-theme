---
title: Premiers pas
type: docs
--- 

# Premiers pas

## Objectifs de l'exercice

Dans cet exercice, vous allez :

- Parcourir le docker hub pour choisir une image
- Télécharger une image dans la base locale
- Démarrer et accéder à un conteneur
- Mettre en place un mapping de port
- Mettre en place des volumes pour stocker les données d'un conteneur de façon persistante


### Présentation 
Nous allons utiliser Docker afin d'installer un serveur web : [nginx](http://nginx.org)

> Votre formateur vous communiquera les informations nécessaires concernant l'ordinateur qui a été provisionné sur aws et sur lequel vous travaillerez


## Un premier conteneur

1. Affichez la version de Docker qui a été installée

    ```bash
    docker version
    ```

1.	En utilisant les informations du site http://hub.docker.com, téléchargez une image contenant la version 1.15.3 du serveur web `nginx`. Consultez la documentation de l'[image docker](https://hub.docker.com/_/nginx/) officielle.

    ```bash
    docker pull ???
    ```

      
    > Vous pouvez maintenant vérifier que l'image est  téléchargée en utilisant la commande `docker images`

    <details>
    <summary>Réponse</summary>
    
    ```bash
    docker pull nginx:1.15.3
    ```
    </details>



1.	Démarrez un conteneur à partir de cette image 

    ```bash
    docker run -name mon-nginx -d ???
    ```
    
    > A quoi sert l'option `-d` ? 

    > la valeur retournée par la commande précédente s'appelle le `CONTAINER_ID`, cette valeur est aussi accessible en version raccourcies en utilisant `docker ps`

1. Utilisez et analysez la commmande `docker inspect CONTAINER_ID`

    > vous modifierez évidemment la variable `CONTAINER_ID` par la valeur précédemment retournéecurl .

1. Que retourne la commande `docker inspect -f "{{ .NetworkSettings.IPAddress }}"  $CONTAINER_ID` ?

1. Commentez le résultat de la commande suivante :

    ```bash
    curl $CONTAINER_IP_ADDRESS:80
    ```

    > Cette solution ne fonctionne pas sur Windows ou OSX, pourquoi ?

    <details>
    <summary>Réponse</summary>
    
    La commande précédente sélectionne l'adresse IP qui a été attribuée au conteneur lors de son initialisation par Docker. Cette adresse IP est visible dans le hôte qui héberge le conteneur. Si vous utilisez Docker sur OSX ou sur Windows, ce n'est pas votre OS qui démarre le conteneur, mais une machine virtuelle Linux installée en même temps que Docker. D'un point de ve réseau, cette VM fait rempart à votre conteneur. Remarque : il devient maintenant possible de crééer des conteneurs windows qui pourront s'exécuter dans un hote Windows.

    <img src="../images/containers.png" width=700/>
    
    </details>


1. Demandez l'adresse IP du conteneur `nginx` que votre voisin vient de démarrer. Que constatez vous ?  


1. En utilisant l'option `--publish` ou son raccourcie `-p`, faîtes en sorte que `curl localhost:8081` fonctionne...

    > N'essayez pas d'utiliser le port 8080, ce dernier est déja pris par une autre application sur la machine que nous avons mis à votre disposition

    > [Documentation](https://docs.docker.com/config/containers/container-networking/)


## Persister les données d'un conteneur

Comme vous pouvez l'imaginer, les données stockées dans une image ne sont pas durables, et sont liées au cycle de vie du conteneur. La suppression de ce dernier provoquera la perte des données qu'il aurait pu stocker sur son espace disque.

Le principe fondamental utilisé par Docker pour accéder à des données externes à un conteneur est d'utiliser un point de montage.
Ainsi, docker vous donne la possibilité de rajouter un répertoire dans le conteneur, ce répertoire étant finalement un lien vers un stockage extérieur. Deux formes exsitent :

- **bind mount** : Un répertoire appartenant au docker host
- **Volume docker** : une ressource dont le cycle de vie est lui même géré via docker (il s'agit d'un "disque dur virtuel"), de façon décorélé de celui du conteneur auquel on l'associe.

### Bind Mount

Créez les ressources suivantes sur le docker host : 

```bash
mkdir ~/public_html
echo "Hello World" >> ~/public_html/hello.html
```
En utilisant la documentation officielle de docker, démarrez un conteneur à partir de l'image `nginx`et faites en sorte que le répertoire `/usr/share/nginx/html` de ce conteneur référence le répertoire `~/public_html`

- Vérifiez que votre page hello.html s'affiche bien à l'adresse suivants : http://CONTENEUR_IP/hello.html

- Utilisez la commande suivante pour afficher le contenu du répertoire `/usr/share/nginx/html` du conteneur précédemment créé : 
    ```bash
    docker exec CONTENEUR_ID ls /usr/share/nginx/html
    ```

- Que se passe t'il si vous rajoutez un fichier dans le répertoire `~/public_html` du docker host ?

- Analysez le contenu du fichier `/etc/nginx/conf.d/default.conf` (et plus précisément la 10ème ligne _location_) et justifiez le choix du point de montage dans le conteneur

    ```bash
    docker exec CONTENEUR_ID ls /etc/nginx/conf.d/default.conf
    ```

### Volume

Le problème du **bind mount** vu précédemment est le lien physique qu'il représente avec le doker host.

Docker préconise donc l'usage des `volumes`.

- Un volume est une ressource entièrement gérée par docker : 

    ```bash
    docker volume create my-data
    docker volume ls
    docker volume rm my-data
    ```

- Créez le volume `public_html` (nous choisissons volontairement de nommer le volume de façon identique au répertoire précédemment créé, c'est un choix, pas du tout une obligation)

- Expliquez ce que fait le traitement suivant : 

    ```bash
    docker run -d --rm --name toto -v public_html:/data alpine 
    docker copy ~/public_html/* toto:/data
    docker stop toto
    docker run -v public_html:/data alpine ls /data
    ```
 
- Montez le volume précédemment créé sur une image **nginx**.