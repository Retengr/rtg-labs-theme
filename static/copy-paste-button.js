// ==UserScript==
// @name        GitHub Copy Code Snippet
// @version     0.3.5
// @description A userscript adds a copy to clipboard button on hover of markdown code snippets
// @license     MIT
// @author      Rob Garrison
// @namespace   https://github.com/Mottie
// @include     https://github.com/*
// @include     https://gist.github.com/*
// @run-at      document-idle
// @grant       GM_addStyle
// @require     https://greasyfork.org/scripts/28721-mutations/code/mutations.js?version=666427
// @icon        https://github.githubassets.com/pinned-octocat.svg
// ==/UserScript==
(() => {
	"use strict";
    
	let copyId = 0;
	const markdownSelector = ".markdown-body, .markdown-format, article",
		    codeSelector = "pre:not(.gh-csc-pre)",
            copyButton = document.createElement("clipboard-copy");

	copyButton.className = "btn btn-sm btn-blue tooltipped tooltipped-w gh-csc-button";
	copyButton.setAttribute("aria-label", "Copy to clipboard");
	// This hint isn't working yet (GitHub needs to fix it)
	copyButton.setAttribute("data-copied-hint", "Copied!");
	copyButton.innerHTML = `
		<svg aria-hidden="true" class="octicon octicon-clippy" height="16" viewBox="0 0 14 16" width="14">
			<path fill-rule="evenodd" d="M2 13h4v1H2v-1zm5-6H2v1h5V7zm2 3V8l-3 3 3 3v-2h5v-2H9zM4.5 9H2v1h2.5V9zM2 12h2.5v-1H2v1zm9 1h1v2c-.02.28-.11.52-.3.7-.19.18-.42.28-.7.3H1c-.55 0-1-.45-1-1V4c0-.55.45-1 1-1h3c0-1.11.89-2 2-2 1.11 0 2 .89 2 2h3c.55 0 1 .45 1 1v5h-1V6H1v9h10v-2zM2 5h8c0-.55-.45-1-1-1H8c-.55 0-1-.45-1-1s-.45-1-1-1-1 .45-1 1-.45 1-1 1H3c-.55 0-1 .45-1 1z"></path>
        </svg>`;
        
    //copyButton.innerHTML = `<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 477.867 477.867" style="enable-background:new 0 0 477.867 477.867;" xml:space="preserve"><g><g><path d="M341.333,85.333H51.2c-28.277,0-51.2,22.923-51.2,51.2v290.133c0,28.277,22.923,51.2,51.2,51.2h290.133c28.277,0,51.2-22.923,51.2-51.2V136.533C392.533,108.256,369.61,85.333,341.333,85.333z M358.4,426.667c0,9.426-7.641,17.067-17.067,17.067H51.2c-9.426,0-17.067-7.641-17.067-17.067V136.533c0-9.426,7.641-17.067,17.067-17.067h290.133c9.426,0,17.067,7.641,17.067,17.067V426.667z"/></g></g><g><g><path d="M426.667,0h-307.2c-28.277,0-51.2,22.923-51.2,51.2c0,9.426,7.641,17.067,17.067,17.067S102.4,60.626,102.4,51.2s7.641-17.067,17.067-17.067h307.2c9.426,0,17.067,7.641,17.067,17.067v307.2c0,9.426-7.641,17.067-17.067,17.067s-17.067,7.641-17.067,17.067s7.641,17.067,17.067,17.067c28.277,0,51.2-22.923,51.2-51.2V51.2C477.867,22.923,454.944,0,426.667,0z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>`;




	function addButton(wrap, code) {
		if (!wrap.classList.contains("gh-csc-wrap")) {
			copyId++;
			// See comments from sindresorhus/refined-github/issues/1278
			code.id = `gh-csc-${copyId}`;
			copyButton.setAttribute("for", `gh-csc-${copyId}`);
			wrap.classList.add("gh-csc-wrap");
			wrap.insertBefore(copyButton.cloneNode(true), wrap.childNodes[0]);
		}
	}

	function init() {
		const markdown = document.querySelector(markdownSelector);
		if (markdown) {
			[...document.querySelectorAll(markdownSelector)].forEach(md => {
				[...md.querySelectorAll(codeSelector)].forEach(pre => {
					let code = pre.querySelector("code");
					let wrap = pre.parentNode;
					if (code) {
						// pre > code
						addButton(pre, code);
					} else if (wrap.classList.contains("highlight")) {
						// div.highlight > pre
						addButton(wrap, pre);
					}
				});
			});
		}
	}

	document.addEventListener("ghmo:container", init);
	document.addEventListener("ghmo:comments", init);
	init();
})();


/* GitHub mutations observer library script v0.3.0
 * Detect changes to various elements and trigger an event
 * This script is meant to be used as a library for GitHub-based userscripts
 * Copyright © 2018 Rob Garrison
 * License: MIT
 */
(() => {
	"use strict";

	// prefix for event & document body class name, e.g. "ghmo:container"
	const prefix = "ghmo",
		disableAttr = `data-${prefix}-disable`,
		debounceInterval = 200,
		targets = {
			// pjax container (covers general, repo & gists)
			// .news = newsfeed layout
			"[data-pjax-container], .news": {
				count: 0,
				name: "container"
			},
			// comment preview active
			".js-preview-body": {
				count: 0,
				name: "preview"
			},
			// .js-discussion = wrapper for progressively loaded comments;
			// "# items not shown" example: https://github.com/isaacs/github/issues/18
			// .discussion-item = issue status changed (github-issue-show-status)
			// #progressive-timeline-item-container = load hidden items (old?)
			// #js-progressive-timeline-item-container = load hidden items
			".js-discussion, .discussion-item, .toolbar-item, #progressive-timeline-item-container, #js-progressive-timeline-item-container": {
				count: 0,
				name: "comments"
			},
			// progressively loaded content (diff files)
			".js-diff-progressive-container, .data.blob-wrapper, .js-diff-load-container, .diff-table tbody": {
				count: 0,
				name: "diff"
			}
		},
		list = Object.keys(targets);

	function fireEvents() {
		list.forEach(selector => {
			if (targets[selector].count > 0) {
				// event => "ghmo:container", "ghmo:comments"
				const event = new Event(prefix + ":" + targets[selector].name);
				document.dispatchEvent(event);
			}
			targets[selector].count = 0;
		});
	}

	function init() {
		// prevent error when library is loaded at document-start & no body exists
		const container = document.querySelector("body");
		let timer;
		// prevent script from installing more than once
		if (container && !container.classList.contains(prefix + "-enabled")) {
			container.classList.add(prefix + "-enabled");
			// bound to document.body... this may be bad for performance
			// http://stackoverflow.com/a/39332340/145346
			new MutationObserver(mutations => {
				clearTimeout(timer);
				/* document.body attribute used to disable updates; it *should not*
				 * be used regularly as multiple scripts may enable or disable the
				 * observers at inappropriate times. It is best that each script handles
				 * the mutation events triggered by this library on its own
				 */
				if (container.getAttribute(disableAttr)) {
					return;
				}
				let mindx, target, lindx,
					llen = list.length,
					mlen = mutations.length;
				// avoiding use of forEach loops for performance reasons
				for (mindx = 0; mindx < mlen; mindx++) {
					target = mutations[mindx].target;
					if (target) {
						for (lindx = 0; lindx < llen; lindx++) {
							if (target.matches(list[lindx])) {
								targets[list[lindx]].count++;
							}
						}
					}
					timer = setTimeout(() => {
						fireEvents();
					}, debounceInterval);
				}
			}).observe(container, {
				childList: true,
				subtree: true
			});
		}
	}

	if (document.readyState === "loading") {
		document.addEventListener("DOMContentLoaded", () => init);
	} else {
		init();
	}

})();